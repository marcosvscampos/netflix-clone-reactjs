import React from "react";
import "./FeaturedMovie.css";

const FeaturedMovie = ({ item }) => {
  console.log(item);
  let firstAirDate = new Date(item.first_air_date);
  let genres = item.genres.map((value) => value["name"]).join(", ");
  let description = item.overview;
  if(description.length > 200){
    description = description.substring(0, 250) + '...';
  }

  return (
    <section
      className="featuredMovie"
      style={{
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundImage: `url(https://image.tmdb.org/t/p/original${item.backdrop_path})`,
      }}
    >
      <div className="featured--vertical">
        <div className="featured--horizontal">
          <div className="featured--name">{item.original_name}</div>
          <div className="featured--info">
            <div className="featured--points">
              {item.vote_average} <span>pontos</span>
            </div>
            <div className="featured--year">{firstAirDate.getFullYear()}</div>
            <div className="featured--seasons">
              {item.number_of_seasons} temporada
              {item.number_of_seasons !== 1 ? "s" : ""}
            </div>
          </div>
          <div className="featured--description">{description}</div>
          <div className="featured--buttons">
            <a className="featured--watchButton" href={`/watch/${item.id}`}>
              &#9658; Assistir
            </a>
            <a className="featured--myListButton" href={`/list/add/${item.id}`}>
              &#43; Minha Lista
            </a>
          </div>
          <div className="featured--genres">
            <strong>Generos:</strong> <span>{genres}</span>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FeaturedMovie;
