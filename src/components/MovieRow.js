import React, { useState } from "react";
import "./MovieRow.css";

import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";

const buildImageUrl = (imgPath) => {
  if (!imgPath) {
    return "";
  }

  return `https://image.tmdb.org/t/p/w300${imgPath}`;
};

const MovieRow = ({ title, items }) => {
    const [scrollX, setScrollX] = useState(0);
    
    const handleLeftArrow = () => {
        let x = scrollX + Math.round(window.innerWidth / 2);
        if(x > 0) {
            x = 0
        }
        setScrollX(x);
    }

    const handleRightArrow = () => {
        const windowInnerWidth = window.innerWidth;
        let x = scrollX - Math.round(windowInnerWidth / 2);
        let totalWidth = items.results.length * 150;
        if((windowInnerWidth - totalWidth) > x) {
            x = (windowInnerWidth - totalWidth) - 60;
        }
        
        setScrollX(x); 
    }
  
    return (
    <div className="movieRow">
      <h2>{title}</h2>

      <div className="movieRow--left">
        <NavigateBeforeIcon style={{ fontSize: 50 }} onClick={handleLeftArrow}/>
      </div>

      <div className="movieRow--right" onClick={handleRightArrow}>
        <NavigateNextIcon style={{ fontSize: 50 }} />
      </div>

      <div className="movieRow--listArea">
        <div className="movieRow--list" style={{ 
            marginLeft: scrollX, 
            width: items.results.length * 150
            }}>
          {items.results.length > 0 &&
            items.results.map((item, key) => (
              <div className="movieRow--item">
                <img
                  src={buildImageUrl(item.poster_path)}
                  alt={item.original_title}
                />
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default MovieRow;
