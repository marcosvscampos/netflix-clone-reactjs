import React from "react";
import "./Header.css";
import NetflixLogo from "../images/Logo_Netflix.png";
import NetflixUserAvatar from "../images/Netflix_User_Avatar.png";

const Header = ({ shadedHeader }) => {
  return (
    <header className={shadedHeader ? "black" : ""}>
      <div className="header--logo">
        <a href="/">
          <img src={NetflixLogo} alt="Netflix" />
        </a>
      </div>
      <div className="header--user">
        <a href="/">
          <img src={NetflixUserAvatar} alt="User Avatar" />
        </a>
      </div>
    </header>
  );
};

export default Header;
