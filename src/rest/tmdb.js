const API_KEY = 'e3aaa8c665fb07bb012b783da4949936';
const API_BASE_PATH = 'https://api.themoviedb.org/3';
const LANGUAGE = 'pt-BR';

const baseFetch = async(endpoint, query) => {
    let queryParam = (query === undefined ? `` : query); 
    const req = await fetch(`${API_BASE_PATH}${endpoint}?${queryParam}&api_key=${API_KEY}&language=${LANGUAGE}`)
    return await req.json();
}

export default {
    getMediaInfo: async(id, type) => {
        let info = {};
        
        if(id) {
            switch(type) {
                case 'movie':
                    info = baseFetch(`/movie/${id}`);
                    break;
                case 'tv':
                    info = baseFetch(`/tv/${id}`);
                    break;
            }
        }

        return info;
    },

    getHomeList: async() => {
        return [
            {
                slug: 'originals',
                title: 'Netflix Originals',
                items: await baseFetch(`/discover/tv`, `with_network=213`)
            },
            {
                slug: 'trending',
                title: 'Trending for you',
                items: await baseFetch(`/trending/all/week`)
            },
            {
                slug: 'toprated',
                title: 'Weekly Top Rated',
                items: await baseFetch(`/movie/top_rated`)
            },
            {
                slug: 'action',
                title: 'Action',
                items: await baseFetch(`/discover/movie`, `with_genres=28`)
            },
            {
                slug: 'comedy',
                title: 'Comedy',
                items: await baseFetch(`/discover/movie`, `with_genres=35`)
            },
            {
                slug: 'horror',
                title: 'Horror',
                items: await baseFetch(`/discover/movie`, `with_genres=27`)
            },
            {
                slug: 'romance',
                title: 'Romance',
                items: await baseFetch(`/discover/movie`, `with_genres=10749`)
            },
            {
                slug: 'documentary',
                title: 'Documentary',
                items: await baseFetch(`/discover/movie`, `with_genres=99`)
            },
        ]
    }
}