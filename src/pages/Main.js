import React, {useState, useEffect} from "react";

import MovieRow from "../components/MovieRow";
import FeaturedMovie from "../components/FeaturedMovie";
import Header from "../components/Header";
import TMDBRequest from "../rest/tmdb.js";

import NetflixLoading from "../images/netflix-loading.gif";

const Main = () => {
  const [movieList, setMovieList] = useState([]);
  const [featuredData, setFeaturedData] = useState(null);
  const [isShadedHeader, setIsShadedHeader] = useState(false);

  useEffect(() => {
    const loadAll = async () => {
      let list = await TMDBRequest.getHomeList();
      setMovieList(list);

      let originals = list.filter((i) => i.slug === "originals");
      let randomChosen = Math.floor(
        Math.random() * originals[0].items.results.length
      );
      let chosen = originals[0].items.results[randomChosen];
      let chosenInfo = await TMDBRequest.getMediaInfo(chosen.id, "tv");
      setFeaturedData(chosenInfo);
    };

    loadAll();
  }, []);

  useEffect(() => {
    const scrollListener = () => {
      if (window.scrollY > 5) {
        setIsShadedHeader(true);
      } else {
        setIsShadedHeader(false);
      }
    };

    window.addEventListener("scroll", scrollListener);

    return () => {
      window.removeEventListener("scroll", scrollListener);
    };
  }, []);

  return (
    <div className="page">
      <Header shadedHeader={isShadedHeader} />
      {featuredData && <FeaturedMovie item={featuredData} />}
      <section className="lists">
        {movieList.map((item, key) => (
          <MovieRow key={key} title={item.title} items={item.items} />
        ))}
      </section>

      {movieList <= 0 && (
        <div className="loading">
          <img src={NetflixLoading} alt="Carregando" />
        </div>
      )}
    </div>
  );
};

export default Main;
